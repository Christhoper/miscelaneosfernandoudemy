import { Routes } from '@angular/router';

import { UserEditComponent } from './user-edit.component';
import { UserDetailComponent } from './user-detail.component';
import { UserNewComponent } from './user-new.component';

export const USER_ROUTES: Routes = [
    { path: 'userNew', component: UserNewComponent },
    { path: 'userEdit', component: UserEditComponent },
    { path: 'userDetail', component: UserDetailComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'userNew' },

    { path: '**', pathMatch: 'full', redirectTo: 'home' },

];